export default  function Config($locationProvider, $provide, $stateProvider,cfpLoadingBarProvider) {
  	$locationProvider.html5Mode(true);
    cfpLoadingBarProvider.includeSpinner = true;

};
Config.$inject = ['$locationProvider', '$provide', '$stateProvider','cfpLoadingBarProvider'];