export default  function routeConfig($stateProvider, $urlRouterProvider) {
    $urlRouterProvider
        .otherwise('/user/list');

    $stateProvider
        // .state('login', {
        //     url: '/login',
        //     templateUrl: 'app/auth/login.html',
        //     controller: 'AuthCtrl',
        //     controllerAs: '$auth'
        // })

        // .state('forgot',{
        //     url:'/forgot',
        //     templateUrl: 'app/auth/forgot-password.html',
        //     controller: 'AuthCtrl',
        //     controllerAs: '$auth'
        // })

        .state('app', {
            url: '/',
            templateUrl: 'app/layout/app.html',
            controller: 'AppCtrl',
            controllerAs: '$app'
        })

        // .state('app.dashboard', {
        //     url: 'dashboard',
        //     template: '<ui-view></ui-view>',
        //     controller: 'DashCtrl',
        //     controllerAs: '$dash',
        //     resolve:{
        //         dashboardlist: ['dataservice', function (dataservice) {
        //             return dataservice.dashboardList()
        //         }]
        //     }
        // })

        // .state('app.dashboard.list',{
        //     url: '/list',
        //     templateUrl: 'app/dashboard/dashboard.html'
        // })
        .state('app.user', {
            url: 'user',
            template: '<ui-view></ui-view>',
            controller: 'UserCtrl',
            controllerAs: '$usr',
            resolve:{
                userlist: ['dataservice', function (dataservice) {
                    return dataservice.userlist()
                }]
            }
        })

        .state('app.user.list',{
            url: '/list',
            templateUrl: 'app/user/user-list.html'
        })

}

routeConfig.$inject = ["$stateProvider", "$urlRouterProvider"];