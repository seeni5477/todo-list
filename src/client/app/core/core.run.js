export default  function Run($rootScope, $state, $location, $cookies,$httpProvider,dataservice,$interval,configureDefaults) {
    $rootScope.$on("$stateChangeStart",
        function (e, toState, toParams, fromState) {

            if (toState.name == 'app') {
                e.preventDefault();
                $state.transitionTo('app.user.list');
            }
        });
}

Run.$inject = ['$rootScope', '$state', '$location', '$cookies', 'dataservice','$interval'];