export default  function DataService($http) {

    var userlist = function() {
        console.log('enter dataservice.js')
        return $http({
            method: 'GET',
            url: '/api/v1/user/list'
        })
            .then(success)
            .catch(fail);
    };

    var userAddOrUpdate = function(data) {
        return $http({
            method: 'POST',
            url: '/api/v1/user/add/update',
            data: data
        })
            .then(success)
            .catch(fail)
    }

    var deleteUser = function(data){
        return $http({
            method: "DELETE",
            url: '/api/v1/user/delete',
            data: data,
            headers:{'Content-Type': 'application/json;charset=utf-8'}
        })
        .then(success)
        .catch(fail);
    };

    function success(response) {
        return response.data;
    }
    function fail(e) {
        console.log(e);
        return;
    }

    this.service = {
        userlist: userlist,
        userAddOrUpdate: userAddOrUpdate,
        deleteUser : deleteUser,
    };

    return this.service;
}



DataService.$inject = ["$http"];