export default class AuthCtrl {
	 constructor($q, dataservice, $state, $mdDialog, toastr, $timeout, $cookies,$location,$scope,$rootScope,$window,$http) {
	    this._$q = $q;
	    this.dataservice = dataservice;
	    this.$state = $state;
      this.$mdDialog = $mdDialog;
	    this.toastr = toastr;
	    this.$timeout = $timeout;
	    this.$cookies = $cookies;
	    this.$location = $location;
	    this.$scope = $scope;
	    this.$rootScope = $rootScope;
	    this.$window = $window;
      this.$http = $http;
      console.log("this.$http");
    }

    login(user){
  		var self = this;
        self.toastr.clear();
        self.$scope.disable = true;
  	    self.dataservice.login(user).then(result=>{
            console.log(result);
            if(result.status== true){
                self.$cookies.put('_id', result.data._id);
                self.$cookies.put('emp_id', result.data.emp_id);
                self.$cookies.put('email', result.data.username);
                self.$cookies.put('role', result.data.role);
                self.$cookies.put('status', result.data.active);
                self.toastr.success("Congrats you logged In", 'Success',{timeOut:5000});
                self.$timeout(function(){
                    self.$state.transitionTo('app.dashboard.list');
                    self.$scope.disable = false;
                },1000);
  	  	    }else{
                self.toastr.warning(result.msg, 'Warning');
                self.$timeout(function(){
                    self.$scope.disable = false;
                },5000);
            }
  	    },err=>{
            self.$scope.disable = false;
            self.toastr.error("Please check your connection!", 'Error');
  	    })
    };
	

}