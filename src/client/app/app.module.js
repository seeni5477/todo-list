import angular from 'angular';
import uiRoute from 'angular-ui-router';
import toastr from 'angular-toastr';
import cookies from 'angular-cookies';
import ngAnimate from 'angular-animate';
import ngAria from 'angular-aria';
import ngMaterial from 'angular-material';
import uiMask from 'angular-ui-mask';

import 'angular-toastr/dist/angular-toastr.css';
import 'angular-material/angular-material.css';
import 'angular-loading-bar/build/loading-bar.css'

import 'angular/angular.js';
import 'angular-ui-bootstrap/dist/ui-bootstrap.js';
import 'angular-ui-bootstrap/dist/ui-bootstrap-tpls.js';
import 'angular-loading-bar/build/loading-bar.js';
import 'angularjs-social-login/angularjs-social-login.js'

import Config from './core/core.config.js';
import Router from './core/core.router.js';
import Run from './core/core.run.js';
import DataService from './core/dataservice.js';
import UserCtrl from './user/user.controller.js';
import AuthCtrl from './auth/auth.controller.js';
import AppCtrl from './layout/app.controller.js';

let appModule = angular.module('todolist', ['ui.bootstrap','angular-loading-bar','socialLogin', uiRoute, toastr, cookies, ngMaterial, ngAnimate, ngAria,uiMask]);

appModule.run(Run);
appModule.config(Config);
appModule.config(Router);
appModule.factory('dataservice', DataService);
appModule.controller('UserCtrl', UserCtrl);
appModule.controller('AuthCtrl', AuthCtrl);
appModule.controller('AppCtrl', AppCtrl);


export default appModule;