export default class UserCtrl {
	 constructor($q, dataservice, $state, $mdDialog, userlist, toastr, $timeout, $cookies,$scope,$rootScope) {
	    this._$q = $q;
	    this.dataservice = dataservice;
	    this.$state = $state;
        this.$mdDialog = $mdDialog;
	    this.toastr = toastr;
	    this.$timeout = $timeout;
	    this.$cookies = $cookies;
	    this.$scope = $scope;
	    this.$rootScope = $rootScope;
        this.userlist = userlist.data ? userlist.data: [];
        this.user = {};
        console.log("this.userlist", this.userlist);
	 }

    openModel() {
      $('#edit').addClass('in');
    };

    addOrUpdateUser() {
        var self = this;
        console.log("self.user", self.user);
        self.dataservice.userAddOrUpdate(self.user).then(resp=> {
            console.log("resp", resp);
            if (resp.status == true) {
            self.toastr.success("Added Successfully", 'Success');
            self.$state.go('app.user.list', {}, {reload: true});
            $('#edit').removeClass('in');
            } else {
            self.toastr.error(resp.msg, 'Error');
            }
        
        }, err=> {
            self.toastr.error("Please check your connection!", 'Error');
        })
    };

    deleteUser(ev, id) {
      var self = this;
      var confirm = self.$mdDialog.confirm()
            .title('Would you like to delete this employee?')
            .textContent('Once you delete you can\'t revert')
            .targetEvent(ev)
            .ok('Ok')
            .cancel('Cancel');
        self.$mdDialog.show(confirm).then(function () {
            self.dataservice.deleteUser({_id: id}).then(resp=> {
                console.log("resp", resp);
                if (resp.status == true) {
                    self.toastr.success("Deleted Successfully", 'Success');
                    self.$state.go('app.user.list',{}, {reload: true});
                } else {
                    self.toastr.error(resp.msg, 'Error');
                }
            },err=>{
                self.toastr.error("Please check your connection!", 'Error');
            })
        });
    };

	

}