export default class AppCtrl {
	constructor($q, $cookies, $state,toastr,$timeout, $rootScope, $scope, dataservice) {
		this._$q = $q;
	    this.$cookies = $cookies;
	    this.$state = $state;
	    this.toastr = toastr;
	    this.$timeout = $timeout;
	    this.$rootScope = $rootScope;
	    this.$scope = $scope;
	    this.dataservice = dataservice;
	}

  	logOut() {
        var self = this;
        var cookies = self.$cookies.getAll();
        angular.forEach(cookies, function (v, k) {
		    self.$cookies.remove(k);
		});
        this.toastr.success('You have logged out successfully!!','Success');
		self.$state.transitionTo('login');
	};



}