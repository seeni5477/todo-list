const os = require('os');

var config = {};
process.env.NODE_ENV = "dev";

config.dev = {
	db : 'mongodb://localhost:27017/todo-list',
	uploads_dir_path : `${os.homedir()}/todo_list_uploads`,
	host : "http://localhost",
	node_port : 8890,
	browser_sync_port : 3000
};

module.exports = config;