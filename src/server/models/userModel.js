var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var userSchema = new Schema({
    name                : {type: String},
    email_id            : {type: String},
    phone_no           	: {type: Number},
    address             : {type: String},
});

module.exports= mongoose.model("users", userSchema, "users");