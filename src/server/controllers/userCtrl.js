var userService = require('../services/userService.js');

var userMaster = {};

userMaster.addOrUpdateUser = function(req, res) {
	userService.findOneAndUpdate(req.body).then(resp=> {
		res.send({
			status: true,
			data: resp
		});
	}).catch(err=> {
		res.send({
			status: false,
			msg: 'Unexpected Error'
		});
	})
};

userMaster.getAllUsers = function(req, res) {
	userService.findAllUser().then(resp=> {
		res.send({
			status: true,
			data: resp
		});
	}, err=> {
		res.send({
			status: false,
			msg: 'No Records Found'
		});
	}).catch(err=> {
		res.send({
			status: false,
			msg: 'Unexpected Error'
		});
	})
}

userMaster.deleteUser = function(req, res) {
	userService.findByIdAndRemove(req.body).then(resp=> {
		res.send({
			status: true,
			data: resp
		});
	},err=> {
		res.send({
			status: false,
			msg: 'Invalid Request'
		});
	}).catch(err=> {
		res.send({
			status: false,
			msg: 'Unexpected Error'
		});
	})
};


module.exports = userMaster;