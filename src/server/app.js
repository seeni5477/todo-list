const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
var timestamps = require('mongoose-timestamp');
mongoose.plugin(timestamps,  {
  createdAt: 'created_at',
  updatedAt: 'updated_at'
});
const path = require("path");
const fs = require("fs");
const favicon = require('serve-favicon');
const multiparty = require('connect-multiparty');
process.env.NODE_ENV = 'development';
const config = require('./config/config')[process.env.NODE_ENV || "dev"];

const port = config.node_port;

mongoose.Promise = global.Promise;

app.use(favicon(__dirname + '/fav.ico'));
app.use(bodyParser.urlencoded({extended: true,limit:'50mb'}));
app.use(bodyParser.json({limit:'50mb'}));
app.use(express.static('../../'));
app.use(express.static('../client/'));

var routes = require('./router')(app);
app.use('/*', express.static('../client/index.html'));

mongoose.connect(`${config.db}`,{ useNewUrlParser: true }).then(
    () => {
        console.log("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        console.log(' Database connected as successfully');
        console.log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
    },
    err => {
        console.log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        console.log(' Database connection Error');
        console.log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
    }
);
var connection = mongoose.connection;

app.listen(port,function () {
    console.log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
    console.log(' SERVER is listening the following port number: ' + port);
    console.log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

});