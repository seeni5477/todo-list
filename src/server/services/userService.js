var userModel = require('../models/userModel.js');

var userMaster = {};

userMaster.findOneAndUpdate = (req)=> {
    return new Promise((resolve, reject)=> {
        try {
            if (!req._id) {
                req = new userModel(req);
            }
            console.log("req", req);
            userModel.findOneAndUpdate({_id: req._id}, req, {new: true, upsert: true}, function(err, data) {
                if (err || !data) {
                    return reject(err);
                } else {
                    resolve(data);
                }
            })
        } catch(err) {
            return reject(err);
        }
    })
};


userMaster.findAllUser = function(req) {
  return new Promise((resolve, reject)=> {
    try {
        userModel.find({})
        .sort({create_at: -1})
        .exec(function(err,user) {
            if(err || !user.length){
                return reject(err);
            } else {
                resolve(user);
            }
        })
    } catch(err) {
        return reject(err);
    }
  })  
};

userMaster.findByIdAndRemove = function(req) {
  return new Promise((resolve, reject)=> {
    try {
        userModel.deleteOne(req,
            function(err, user) {
                if(err || !user) {
                    return reject(err);        
                } else {
                    resolve(user);
                }

        })
    } catch(err) {
        return reject(err);
    }
  })  
};


module.exports = userMaster;