const path = require('path');
const webpack = require('webpack');

process.env.NODE_ENV = "development";
// process.env.NODE_ENV = "production";
// process.env.NODE_ENV = "none";

module.exports = {
      mode: process.env.NODE_ENV,
      entry: './src/client/app/app.module.js',
      output: {
        path: path.join(__dirname, 'src/client/dist'),
        filename: 'bundle.js'
      },
      devtool: 'source-map',
      module: {
          rules: [
                {
                    test: /\.css$/,
                    loader: ["style-loader","css-loader"]
                },{
                    test: /\.js$/,
                    exclude: /node_modules/,
                    loader: "babel-loader",
                    query: {
                            presets: ['es2015']
                    }
                }
          ]
      }
};