"use strict";
const path = require('path');
const gulp = require('gulp');
const del = require('del');
const webpack = require('webpack-stream');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const less = require('gulp-less');
const cssmin = require('gulp-cssmin');
const exec = require('child_process').exec;
const browserSync = require('browser-sync').create();
const config = require('./src/server/config/config')[process.env.NODE_ENV || "development"];

gulp.task('clean', function() {
  	return del('src/client/dist/*');
});

gulp.task('build', ['clean'],function() {
  	return gulp.src('src/client/app/app.module.js')
			.pipe(webpack( require('./webpack.config.js') ))
			.pipe(gulp.dest('src/client/dist/'));
});

gulp.task('compress', ['build'], function() {
    return gulp.src('src/client/dist/bundle.js')
      .pipe(uglify({mangle: false}))
      .pipe(rename('bundle.min.js'))
      .pipe(gulp.dest('src/client/dist/'));
});

gulp.task('start', ['compress'], function() {
      var proces = exec('cd src/server && node app.js');

      proces.stdout.on('data', function (data) {
        console.log(data.toString());
      });

      proces.stderr.on('data', function (data) {
        console.log(data.toString());
      });

      proces.on('exit', function (code) {
        console.log('process exited with code ' + code.toString());
      });
});

gulp.task('serve-dev', ['start'], function() {
    browserSync.init(null,{
        proxy: `${config.host}:${config.node_port}/`,
        port: config.browser_sync_port,
        notify: true
    });
});

gulp.task('serve-pro', ['compress'], function() {
	    var proces = exec('cd src/server && forever start app.js');

      proces.stdout.on('data', function (data) {
        console.log(data.toString());
      });

      proces.stderr.on('data', function (data) {
        console.log(data.toString());
      });

      proces.on('exit', function (code) {
        console.log('spotikon process exited with code ' + code.toString());
      });
});

//Initial task to call
gulp.task('serve', function() {
    if(process.env.NODE_ENV=="production"){
      gulp.start('serve-pro');
    }else{
      gulp.start('serve-dev');
    }
});

gulp.task('restart', function() {
      var proces = exec('cd src/server && forever restart app.js');

      proces.stdout.on('data', function (data) {
        console.log(data.toString());
      });

      proces.stderr.on('data', function (data) {
        console.log(data.toString());
      });
});

gulp.task('stop', function() {
      var proces = exec('cd src/server && forever stop app.js');

      proces.stdout.on('data', function (data) {
        console.log(data.toString());
      });

      proces.stderr.on('data', function (data) {
        console.log(data.toString());
      });
});